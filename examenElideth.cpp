
// Online C++ Compiler - Build, Compile and Run your C++ programs online in your favorite browser

#include <iostream>
#include <cmath>

using namespace std;

class ConvertidorGradoradi {
public:
    float grados_a_radianes(float grados) {
        if (grados < 0) {
            cerr << "Error: Los grados no pueden ser negativos." << endl;
            return -1.0;
        }
        return grados * (3.1416 / 180.0);
    }

    float radianes_a_grados(float radianes) {
        if (radianes < 0) {
            cerr << "Error: Los radianes no pueden ser negativos." << endl;
            return -1.0;
        }
        return radianes * (180.0 / 3.1416);
    }
};

class ConvertidorDistancias : public ConvertidorGradoradi {
public:
    float kilometros_a_millas(float kilometros) {
        if (kilometros < 0) {
            cerr << "Error: La distancia en kilómetros no puede ser negativa." << endl;
            return -1.0;
        }
        return kilometros * 0.621371;
    }

    float millas_a_kilometros(float millas) {
        if (millas < 0) {
            cerr << "Error: La distancia en millas no puede ser negativa." << endl;
            return -1.0;
        }
        return millas / 0.621371;
    }
};

int main() {
    int opc;
    float value, resultado;

    cout << "Seleccione la conversión que desea realizar:" << endl;
    cout << "1. Grados a Radianes" << endl;
    cout << "2. Radianes a Grados" << endl;
    cout << "3. Kilómetros a Millas" << endl;
    cout << "4. Millas a Kilómetros" << endl;

    cin >> opc;

    ConvertidorGradoradi convertidorGradoradi;
    ConvertidorDistancias convertidorDistancias;

    switch (opc) {
        case 1:
            cout << "Ingrese los grados: ";
            cin >> value;
            resultado = convertidorGradoradi.grados_a_radianes(value);
            if (resultado != -1.0) {
                cout << "El resultado es: " << resultado << " radianes" << endl;
            }
            break;

        case 2:
            cout << "Ingrese los radianes: ";
            cin >> value;
            resultado = convertidorGradoradi.radianes_a_grados(value);
            if (resultado != -1.0) {
                cout << "El resultado es: " << resultado << " grados" << endl;
            }
            break;

        case 3:
            cout << "Ingrese los Kilómetros: ";
            cin >> value;
            resultado = convertidorDistancias.kilometros_a_millas(value);
            if (resultado != -1.0) {
                cout << "El resultado es: " << resultado << " millas" << endl;
            }
            break;

        case 4:
            cout << "Ingrese las millas: ";
            cin >> value;
            resultado = convertidorDistancias.millas_a_kilometros(value);
            if (resultado != -1.0) {
                cout << "El resultado es: " << resultado << " kilómetros" << endl;
            }
            break;

        default:
            cout << "Opción no válida." << endl;
            break;
    }

    return 0;
}
